use nom::{
    branch,
    bytes::complete::{tag, take_until, take_while1},
    character::is_alphanumeric,
    combinator::{eof, map_res, rest},
    error::ErrorKind,
    multi::many_till,
    sequence::delimited,
    IResult,
};

#[derive(PartialEq, Debug)]
pub enum Member {
    Raw(String),
    Var(String),
}

const ESCAPE_START: &str = "<[";
const ESCAPE_END: &str = "]>";

fn raw(input: &str) -> IResult<&str, Member> {
    map_res(
        branch::alt((take_until(ESCAPE_START), rest)),
        |v: &str| -> Result<Member, ErrorKind> { Ok(Member::Raw(String::from(v))) },
    )(input)
}

fn variable(input: &str) -> IResult<&str, &str> {
    take_while1(|s: char| is_alphanumeric(s as u8) || s.eq(&'_'))(input)
}

fn escape_start(input: &str) -> IResult<&str, &str> {
    tag(ESCAPE_START)(input)
}
fn escape_end(input: &str) -> IResult<&str, &str> {
    tag(ESCAPE_END)(input)
}

fn expression(input: &str) -> IResult<&str, Member> {
    map_res(
        delimited(escape_start, variable, escape_end),
        |v| -> Result<Member, ErrorKind> { Ok(Member::Var(String::from(v))) },
    )(input)
}

fn alt(input: &str) -> IResult<&str, Member> {
    branch::alt((expression, raw))(input)
}

fn eat(input: &str) -> IResult<&str, (Vec<Member>, &str)> {
    many_till(alt, eof)(input)
}

pub fn parse(input: &str) -> Vec<Member> {
    let (_, (members, _)) = eat(input).unwrap();
    members
}

#[cfg(test)]
mod tests {

    use super::{parse, Member};

    #[test]
    fn handle_parse() {
        let input = String::from(
            r#"
            # parsing test
            [section]
            raw = <[var1]>
            clr = <[color_red]>
            "#,
        );
        let parsed = parse(&input);
        assert!(
            parsed
                .iter()
                .filter(|member| matches!(member, Member::Raw(v) if v.contains("raw = ")))
                .count()
                == 1
        );
        assert!(
            parsed
                .iter()
                .filter(|member| matches!(member, Member::Var(v) if v.contains("var1")))
                .count()
                == 1
        );

        // Checking that simple brackets are not recognized as escape tag
        assert!(
            parsed
                .iter()
                .filter(|member| matches!(member, Member::Var(v) if v.contains("[section]")))
                .count()
                == 0
        );
    }
}
