//! CLI interface

use clap::{Parser, Subcommand};
use std::path::PathBuf;

use crate::{
    recipe::{
        self,
        hook::{print_result, Hook},
    },
    stack::Stack,
};

#[derive(Parser)]
#[clap(author, version, about)]
pub struct CLI {
    #[clap(subcommand)]
    pub command: Commands,
}

#[derive(Subcommand)]
pub enum Commands {
    /// Collect templates from repos, and apply the defined context to them
    Sync {
        /// Entry recipe
        recipe: String,

        /// Add an entry to the context (as `key:value`)
        #[clap(short, long)]
        context: Option<String>,

        /// Add a repo (as `path`)
        #[clap(short, long)]
        repo: Option<String>,
    },
    /// Fetch recipes data
    Fetch {
        /// Show recipe informations
        recipe: Option<String>,

        /// Get context value by key
        #[clap(short, long)]
        key: Option<String>,
    },
}

fn parse_context<'a>(context: &'a String) -> (&'a str, &'a str) {
    context.split_once(":").unwrap()
}

/// Handle `sync` command
pub fn handle_sync(recipe_name: &str, context: &Option<String>, repo: &Option<String>) {
    // load stack from recipes
    let mut stack = crate::recipe::load(recipe_name);

    // add additional context
    if context.is_some() {
        let (key, value) = parse_context(context.as_ref().unwrap());
        stack.set_context(key, value.to_string());
    }

    // add additional repo
    if repo.is_some() {
        let r = PathBuf::from(repo.as_ref().unwrap());
        stack.add_repo(r);
    }

    let hooks = recipe::hook::load(recipe_name);
    if hooks.hook.is_some() {
        let Hook {
            pre_sync,
            post_sync,
        } = hooks.hook.unwrap();
        // exec pre-sync hook
        if pre_sync.is_some() {
            let output = recipe::hook::exec(&pre_sync.unwrap());
            print_result(&output);
        }

        // synchronize configurations
        stack.sync_and_write();

        // exec post-sync hook
        if post_sync.is_some() {
            let output = recipe::hook::exec(&post_sync.unwrap());
            print_result(&output);
        }
    }
}

/// Handle `fetch` command
pub fn handle_fetch(recipe: Option<&str>, key: Option<&str>) {
    match recipe {
        Some(recipe_name) => {
            let stack = crate::recipe::load(recipe_name).self_substitute();

            // Intercept "key" option and handle it
            if key.is_some() {
                return println!("{}", fetch_key(&stack, key.unwrap()));
            }

            println!();
            // Print templates informations
            print!("Templates ");
            println!("{:#?}", stack.collect());
            println!();

            // Print context informations
            print!("Context ");
            println!("{:#?}", stack.context);
            println!();
        }
        // Print available recipes
        None => {
            println!();
            print!("Recipes ");
            println!("{:#?}", recipe::list());
            println!();
        }
    }
}

/// Fetch context value from stack by key
fn fetch_key(stack: &Stack, key: &str) -> String {
    stack
        .context
        .get(key)
        .expect("Key not found in context")
        .to_string()
}
