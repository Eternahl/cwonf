//! Command parser, split input by spaces, preserve quoted chunks

use std::process::Command;

use nom::{
    branch,
    bytes::complete::{take_till, take_while1},
    character::{
        complete::{char, space0},
        is_space,
    },
    combinator::{self},
    multi::many1,
    sequence::{self},
    IResult,
};

/// Parse raw input into `Command`
pub fn parse_command(raw: &str) -> Command {
    let (_, res) = parse(raw).unwrap();
    let mut splitted = res.into_iter();
    let program = splitted.next().unwrap();
    let mut command = Command::new(program);
    command.args(splitted.collect::<Vec<&str>>());
    command
}

fn parse_quoted(input: &str) -> IResult<&str, &str> {
    let parse_quote = |q| {
        combinator::recognize(sequence::delimited(
            char(q),
            take_till(move |v| v == q),
            char(q),
        ))
    };
    branch::alt((parse_quote('\''), parse_quote('"'), parse_quote('`')))(input)
}

fn parse_sequence(input: &str) -> IResult<&str, &str> {
    take_while1(|v| !is_space(v as u8))(input)
}

fn parse_chunk(input: &str) -> IResult<&str, &str> {
    branch::alt((parse_quoted, parse_sequence))(input)
}

fn parse(input: &str) -> IResult<&str, Vec<&str>> {
    many1(sequence::terminated(parse_chunk, combinator::opt(space0)))(input)
}

#[cfg(test)]
mod tests {

    use super::parse;
    use crate::recipe::hook::parser::parse_quoted;

    #[test]
    fn parse_quoted_ok() {
        assert_eq!("\"shutdown 0\"", parse_quoted("\"shutdown 0\"").unwrap().1);
        assert_eq!("'shutdown 0'", parse_quoted("'shutdown 0'").unwrap().1);
        assert_eq!("`shutdown 0`", parse_quoted("`shutdown 0`").unwrap().1);
    }

    #[test]
    fn parse_raw_command() {
        let input = "sh -c 'ls -l'";
        let (_, res) = parse(input).unwrap();
        assert_eq!(res, vec!["sh", "-c", "'ls -l'"])
    }
}
