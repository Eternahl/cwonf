//! Recipe hooks handler

use crate::{recipe::hook::parser::parse_command, APP_NAME};
use std::{process::Output, str::from_utf8};

mod parser;

#[derive(Debug, Default, PartialEq, serde::Serialize, serde::Deserialize)]
pub struct Hooks {
    pub hook: Option<Hook>,
}

#[derive(Debug, Default, PartialEq, serde::Serialize, serde::Deserialize)]
pub struct Hook {
    /// Pre-sync hook
    #[serde(default)]
    pub pre_sync: Option<String>,
    /// Post-sync hook
    #[serde(default)]
    pub post_sync: Option<String>,
}

pub fn load(recipe_name: &str) -> Hooks {
    confy::load(APP_NAME, recipe_name).unwrap()
}

pub fn exec(raw: &str) -> Output {
    let mut command = parse_command(raw);
    let output = command.output().unwrap();
    println!("hook `{}`", raw);
    output
}

pub fn print_result(output: &Output) {
    let out = from_utf8(&output.stdout).unwrap();
    if !out.is_empty() {
        println!("output `{}`", out);
    };

    let err = from_utf8(&output.stderr).unwrap();
    if !err.is_empty() {
        println!("error `{}`", err);
    };
}

#[cfg(test)]
mod tests {
    use std::str::from_utf8;

    use crate::recipe::hook::exec;

    #[test]
    fn should_exec_hook_with_output() {
        let raw = "echo YO";
        let output = exec(raw);
        let res = from_utf8(&output.stdout).unwrap();
        assert_eq!(res, "YO\n");
    }

    #[test]
    fn should_exec_hook_with_error() {
        let raw = "ls --unknown_arg";
        let output = exec(raw);
        let res = from_utf8(&output.stderr).unwrap();
        assert!(!res.is_empty());
    }
}
