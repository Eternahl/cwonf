//! A recipe represent a configuration file

pub mod hook;

use std::{
    fs,
    path::{Path, PathBuf},
};

use confy::get_configuration_file_path;
use scan_dir::ScanDir;

use crate::{stack::Stack, APP_NAME};

#[derive(Debug, Default, PartialEq, serde::Serialize, serde::Deserialize)]
struct Includer {
    /// A list of repos path
    #[serde(default)]
    pub include: Vec<String>,
}

/// Load stack from a recipe
pub fn load(recipe_name: &str) -> Stack {
    // check if recipe exist
    let path = get_configuration_file_path(APP_NAME, recipe_name).unwrap();
    fs::metadata(&path)
        .expect(format!("Warning, config file {:?} not found !", path.as_os_str()).as_str());

    let stack: Stack = confy::load(APP_NAME, recipe_name).unwrap();
    let includer: Includer = confy::load(APP_NAME, recipe_name).unwrap();

    includer
        .include
        .iter()
        .map(|recipe_name| load(recipe_name))
        .fold(stack, |s1, s2| Stack::merge(s1, s2))
}

/// List available recipes
pub fn list() -> Vec<String> {
    let location = dirs_next::config_dir()
        .unwrap()
        .join(env!("CARGO_PKG_NAME"));
    let mut recipes = Vec::new();
    ScanDir::files()
        .read(location, |iter| {
            for (entry, _name) in iter {
                if entry.path().extension().unwrap() == Path::new("yml").as_os_str() {
                    let path = PathBuf::from(entry.file_name());
                    let name = String::from(path.file_stem().unwrap().to_str().unwrap());
                    recipes.push(name);
                }
            }
        })
        .unwrap();
    recipes
}

#[cfg(test)]
mod tests {
    use super::Includer;
    use crate::{recipe, stack::Stack};
    use confy::ConfyError;
    use std::path::PathBuf;

    fn write_recipes() -> Result<(), ConfyError> {
        let mut entry: Includer = Default::default();
        entry.include.push("light_theme".to_string());

        let mut light_theme: Stack = Default::default();
        light_theme.add_repo(PathBuf::from("~/.config/myconf"));
        light_theme.set_context("tint", "#eee");
        light_theme.set_context("shade", "#111");

        confy::store(env!("CARGO_PKG_NAME"), "light_theme", light_theme).and(confy::store(
            env!("CARGO_PKG_NAME"),
            "entry",
            entry,
        ))
    }

    #[test]
    fn load() {
        write_recipes().unwrap();

        let stack = recipe::load("light_theme");

        assert!(stack.repos.contains(&PathBuf::from("~/.config/myconf")));
    }

    #[test]
    fn load_with_inclusions() {
        write_recipes().unwrap();

        let stack = recipe::load("entry");

        assert_eq!(stack.context.get("tint").unwrap(), "#eee");
    }
}
