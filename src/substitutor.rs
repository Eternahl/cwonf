//! Apply context to templates

mod parser;

use std::collections::HashMap;

use self::parser::{parse, Member};

/// Apply context to a template
pub fn apply(input: String, context: &HashMap<String, String>) -> String {
    let members = parse(&input);
    let result = compose(&members, &context);
    if members.len() == 1 {
        return result;
    }
    apply(result, &context)
}

/// Map and fold members into string
fn compose(members: &Vec<Member>, context: &HashMap<String, String>) -> String {
    members
        .into_iter()
        .map(|member| match member {
            Member::Raw(v) => v,
            Member::Var(v) => context
                .get(v)
                .expect(&format!("Context entry {:?} not found", v)),
        })
        .fold(String::new(), |acc, elm| format!("{}{}", acc, elm))
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use crate::{stack::Stack, substitutor::apply};

    #[test]
    fn handle_apply() {
        let template = String::from(
            r#"[colors]
            background = <[tint]>
            foreground = <[shade]>
            "#,
        );

        let result = apply(
            template,
            &HashMap::from([
                ("shade".to_string(), "#111".to_string()),
                ("tint".to_string(), "#eee".to_string()),
            ]),
        );

        assert_eq!(
            result,
            r#"[colors]
            background = #eee
            foreground = #111
            "#
        );
    }

    #[test]
    fn handle_deep_substitution() {
        let mut palette: Stack = Default::default();
        palette.set_context("hue", "55");

        let mut theme: Stack = Default::default();
        theme.set_context("tint", "HSL(<[hue]>,50,50)");

        let all = Stack::merge(palette, theme);
        let template = String::from("color: <[tint]>");

        assert_eq!(
            String::from("color: HSL(55,50,50)"),
            apply(template, &all.context),
        );
    }
}
