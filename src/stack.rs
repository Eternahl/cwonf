//! A Stack handle context entries and repos template

use std::{
    collections::{HashMap, HashSet},
    fs,
    path::PathBuf,
};

use scan_dir::ScanDir;

use crate::substitutor;

#[derive(Debug, PartialEq, serde::Serialize, serde::Deserialize)]
/// Represent a configurations stack
pub struct Stack {
    /// A map of key/value used in templates
    #[serde(default)]
    pub context: HashMap<String, String>,
    /// A list of repos path
    #[serde(default)]
    pub repos: HashSet<PathBuf>,
}
impl Stack {
    /// Create or mutate context member
    pub fn set_context<S: ToString>(&mut self, key: &str, value: S) {
        self.context.insert(String::from(key), value.to_string());
    }

    /// Push a template to the list of templates
    pub fn add_repo(&mut self, path: PathBuf) {
        self.repos.insert(path);
    }

    /// Create a new stack by merging to others
    pub fn merge(s1: Stack, s2: Stack) -> Stack {
        let mut merged: Stack = Default::default();

        merged.context.extend(s1.context);
        merged.context.extend(s2.context);

        merged.repos.extend(s1.repos);
        merged.repos.extend(s2.repos);

        merged
    }

    /// Collect templates
    pub fn collect(&self) -> Vec<PathBuf> {
        let mut templates = Vec::new();
        for path in &self.repos {
            if !path.exists() {
                continue;
            }
            let walk_result = ScanDir::all().skip_backup(true).walk(path, |iter| {
                for (entry, name) in iter {
                    if name.ends_with(&format!("{}{}", ".".to_string(), env!("CARGO_PKG_NAME"))) {
                        templates.push(PathBuf::from(entry.path()));
                    }
                }
            });

            if let Err(errors) = walk_result {
                for e in errors {
                    println!("Warning {}. Continue scanning...", e);
                }
            }
        }
        templates
    }

    /// Apply context to templates, and optionnally write them in a file
    pub fn sync_and_write(&self) {
        let substitued = self.self_substitute();
        for template in substitued.collect() {
            print!("syncing {:?}...", template);
            let input = fs::read_to_string(&template).unwrap();
            let content = substitutor::apply(input, &self.context);
            print!("writing...");
            fs::write(resolve_target_path(&template), content).unwrap();
            println!(" ✓");
        }
    }

    /// Apply substitution inside current stack
    pub fn self_substitute(&self) -> Stack {
        let serialized = serde_json::to_string(self).unwrap();
        let substitued = substitutor::apply(serialized, &self.context);
        serde_json::from_str(&substitued).unwrap()
    }
}

/// Resolve path of substitued template
fn resolve_target_path(template: &PathBuf) -> PathBuf {
    template
        .parent()
        .unwrap()
        .join(template.file_stem().unwrap())
}

/// Custom default implementation to add a default repo to the user configurations location
impl Default for Stack {
    fn default() -> Stack {
        Stack {
            repos: HashSet::from_iter(vec![dirs_next::config_dir().unwrap()]),
            context: HashMap::default(),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::{collections::HashMap, path::PathBuf};

    use test_dir::{DirBuilder, FileType, TestDir};

    use crate::stack::Stack;

    #[test]
    fn handle_context() {
        let mut stack: Stack = Default::default();
        stack.set_context("hue", "39");

        assert_eq!(stack.context.get("hue").unwrap(), "39");
    }

    #[test]
    fn handle_templates() {
        let mut stack: Stack = Default::default();
        stack.add_repo(PathBuf::from("/my/location"));

        assert!(stack.repos.contains(&PathBuf::from("/my/location")));
    }

    #[test]
    fn handle_collecting_templates_files() {
        let tmp_dir = TestDir::temp()
            .create("test/cwonf_test", FileType::Dir)
            .create("test/1.json.cwonf", FileType::EmptyFile)
            .create("test/cwonf_test/2.cwonf", FileType::EmptyFile);

        let mut stack: Stack = Default::default();
        stack.add_repo(PathBuf::from(tmp_dir.root()));

        let collected = stack.collect();
        assert!(collected.len() >= 2);
        assert!(collected.contains(&PathBuf::from(tmp_dir.path("test/1.json.cwonf"))));
        assert!(collected.contains(&PathBuf::from(tmp_dir.path("test/cwonf_test/2.cwonf"))));
    }

    #[test]
    fn handle_self_substitution() {
        let mut palette: Stack = Default::default();
        palette.set_context("hue", "55");
        palette.set_context("tint", "HSL(<[hue]>,50,50)");

        assert_eq!(
            Stack {
                context: HashMap::from([
                    ("hue".to_string(), "55".to_string()),
                    ("tint".to_string(), "HSL(55,50,50)".to_string()),
                ]),
                ..Default::default()
            },
            palette.self_substitute(),
        );
    }
}
