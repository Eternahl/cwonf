use clap::StructOpt;
use cli::Commands;

mod cli;
mod recipe;
mod stack;
mod substitutor;

static APP_NAME: &str = env!("CARGO_PKG_NAME");

fn main() {
    let cli = cli::CLI::parse();
    match &cli.command {
        Commands::Sync {
            recipe,
            context,
            repo,
        } => cli::handle_sync(recipe, context, repo),
        Commands::Fetch { recipe, key } => cli::handle_fetch(recipe.as_deref(), key.as_deref()),
    }
}
