_Cwonf_ purpose is to synchronize your configurations files with a common context

- [Features](#features)
- [Usage](#usage)
- [Recipes](#recipes)

# Features

- [x] CLI
- [x] Hooks
- [ ] Plugins (rust ?)
  - [ ] System/env vars mirroring
  - [ ] Substitutions expressions (and correct yaml types)

# Usage

##### cwonf configuration file

```yml
# ~/.config/cwonf/light_theme.yml
context:
  shade: "#111"
  tint: "#eee"
repos:
  - ~/.config # by default, including for example
hook:
  post_sync: "swaymsg reload"
```

##### collected template files

```conf
# ~/.config/xmp/settings.conf.cwonf
[colors]
background = <[tint]>
foreground = <[shade]>
```

##### command

`cwonf sync light_theme`

##### result

```conf
# ~/.config/xmp/settings.conf
[colors]
background = #111
foreground = #eee
```

# Recipes

A recipe is nothing more than a cwonf configuration file. It has the _Yaml_ format, with the following properties :

- `context`: structure of `key:value` to substitute inside templates, use it like this: `<[key]>`
- `include`: list of other recipes to include. Cwonf can substitutes himself, that mean, you can use context from included recipes into your current recipe
- `repos`: list of entry path to search for templates (recursive)
- `hook`: structure with optionnal hooks
  - `pre_sync`: command to execute before sync
  - `post_sync`: command to execute after sync
